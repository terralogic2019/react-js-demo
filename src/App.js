import React from 'react';
import logo from './logo.svg';
import './App.css';
import Page1 from "./compoenents/page1/page1.jsx"
import Page2 from "./compoenents/page2/page2.jsx"
import {
  BrowserRouter as Router,
  HashRouter,
  Switch,
  Route,
} from "react-router-dom";

function App() {
  return (
    <Router>
    <HashRouter>
      <Switch>
        <Route path="/" exact component={Page1} />
        <Route path="/page1"  component={Page1} />
        <Route path="/page2" component={Page2} />
      </Switch>
    </HashRouter>
    </Router>
  );
}

export default App;
