import React, { Component } from 'react'
import "./style.scss"
import Child from "./child.jsx"
import img from "../assets/img.png"
import $ from "jquery"
import { Button } from 'react-bootstrap';
import { AiFillDollarCircle } from "react-icons/ai";
var axios = require("axios")

export default class page1 extends Component {

    state = {
        parentVariable: "Initially no data",
        bg: "yellow",

        tableData: [
            {
                name: "vishram"
            },

            {
                name: "guru"
            },

            {
                name: "prajwal"
            },

            {
                name: "pavitra"
            }
        ]

        ,
        api_data: null

    }


    setParentValue = (val) => {
        this.setState({ parentVariable: val })
    }

    componentWillMount() {

        // alert("before document render")

        // alert("before document render\n\n"+$("#root").html())


    }

    async componentDidMount() {

        let ctx = this

        // axios({
        //     method: 'post',
        //     url: ' https://reqres.in/api/users',
        //     data:{
        //         "name": "morpheus",
        //         "job": "leader"
        //     },
        //     responseType: 'stream'
        //   })
        //     .then(function (response) {
        //         ctx.setState({api_data:response.data})
        //     });

        fetch("https://reqres.in/api/users", {
            method: 'post',
            headers: {
                'Content-Type': 'application/json'
                // 'Content-Type': 'application/x-www-form-urlencoded',
              },
            body: JSON.stringify({
                "name": "morpheus",
                "job": "leader"
            })
        })
            .then(res => res.json())
            .then(
                (result) => {
                    this.setState({
                        api_data: result
                    });
                },
                (error) => {
                    console.log("--------------fetch error",error)
                    this.setState({
                        api_data: "Error in getting data"
                    });
                }
            )



        // alert("after document render\n\n"+$("#root").html())
    }


    componentWillUpdate() {

        console.log("when state changes/any dom change-->brfore")
    }

    componentDidUpdate() {
        console.log("when state changes/any dom change")
    }

    componentWillUnmount() {
        alert("before moving to another compoent/page")
    }


    setBg(e) {
        console.log(e.target.value)
        // this.setState({bg:e.target.value})
    }


    setBg2(v1, e) {
        console.log(e.target.value, v1)
        $(e.target).css({ color: e.target.value })
        this.setState({ bg: e.target.value })
    }

    setBg3 = () => {
        console.log(this.refs.color.value)
        this.setState({ bg: this.refs.color.value })
    }

    setBg4 = (e) => {
        console.log(e.target.value)
        this.setState({ bg: e.target.value })
    }


    render() {
        return (
            <div className="page1" style={{ backgroundColor: this.state.bg }}>
                <h1>Page1</h1>
                <Child data={"hi"} data2={"hellow"} callbackFun={this.setParentValue} />
                <br />
                {this.state.parentVariable}<br />
                {this.state.bg}
                {/* <img src={img} width="400" /> */}

                <input type="text" defaultValue="yellow" onChange={this.setBg} /><br />
                <input type="text" defaultValue="yellow" onChange={this.setBg2.bind(this, "hi")} /><br />
                <input type="text" defaultValue="yellow" ref="color" onChange={this.setBg3} /><br />
                <input type="text" defaultValue="yellow" onChange={this.setBg4} /><br />

                {/* May be both require jquery (just research) */}

                {/* It runs for normal boostrap (bootstrap/4) */}

                <button type="button" className="btn btn-primary">Primary</button><br />


                {/* Its run only when react-boostrap installed  (react-bootstrap) */}
                <Button className="btn btn-danger" >Button</Button><br />

                <AiFillDollarCircle className="icon" /><br /><br />
                {this.state.api_data ? JSON.stringify(this.state.api_data) : "Data is null"}



                <table>
                    <thead>
                        <tr>
                            <th>Names</th>
                        </tr>
                    </thead>
                    <tbody>
                        {this.state.tableData.map((curData) => {
                            return <tr>
                                <td>{curData.name}</td>
                            </tr>
                        })}
                    </tbody>
                </table>

            </div>
        )
    }
}
